import os
import sys
import threading
import time
import timeit
from datetime import datetime

import yaml

from src.formatted_logger import formatted_logger
from src.revisions import Revisions
from src.utils import chunkIt, readArticleList


### Wikipedia Format ###
# In wikipedia, modified part are between <ins class=\"diffchange diffchange-inline\">  and  </ins> tags


def getRevisions(articles, startYear, startMonth, endYear, endMonth, outputDir, lang):
    for article in articles:
        rev = Revisions(lang, article, startYear,
                        startMonth, endYear, endMonth)
        rev.extract()
        rev.to_json("{}/{}.json".format(outputDir, article))
        logger.info("Extraction complete: {}".format(article))


if __name__ == "__main__":
    logger = formatted_logger('RevisionExtractor', 'info')

    try:
        confFile = sys.argv[1]
        with open(confFile, 'r') as f:
            config = yaml.load(f, Loader=yaml.FullLoader)
        logger.info("{} succesfully read".format(confFile))
    except:
        logger.error("No config file was given")
        sys.exit()

    language = config['wikipedia']['language']
    articleListPath = config['paths']['articleListPath']
    outputDir = config['paths']['outputDir']
    workers = config['other']['workers']

    startYear = config['dates']['startYear']
    startMonth = config['dates']['startMonth']

    if not language:
        logger.error("No langage in config file")
        sys.exit()

    if not articleListPath:
        logger.error("No articleList in config file")
        sys.exit()

    if not outputDir:
        logger.error("No outputDir in config file")
        sys.exit()

    endYear = config['dates']['endYear']
    endMonth = config['dates']['endMonth']

    if not os.path.exists(outputDir):
        os.mkdir(outputDir)
        logger.info("Directory {} created ".format(outputDir))
    else:
        logger.info("Directory {} already exists".format(outputDir))

    articles = readArticleList(articleListPath)

    startTime = timeit.default_timer()

    articles_chunk = chunkIt(articles, workers)
    startDate = "{}-{}".format(startYear, startMonth)
    if endYear and endMonth:
        endDate = "{}-{}".format(endYear, endMonth)
    else:
        endDate = "{}-{}".format(datetime.now().year, datetime.now().month)
    logger.info("Start extracting revisions of {} articles from {} to {} - Workers: {}".format(
        len(articles), startDate, endDate, workers))
    threads = list()
    for index in range(workers):
        th = threading.Thread(target=getRevisions, args=(
            articles_chunk[index], startYear, startMonth, endYear, endMonth, outputDir, language,))
        threads.append(th)
        th.start()
        time.sleep(1)  # Delay next API call

    for index in range(workers):
        threads[index].join()

    execTime = timeit.default_timer() - startTime
    logger.info("Extraction finished in {0:.1f} sec".format(execTime))
