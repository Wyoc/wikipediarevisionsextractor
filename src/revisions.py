import urllib.request
import re
import json
import ast
from nested_lookup import nested_lookup
from pprint import pprint


class Revisions():
    def __init__(self, lang, pageName, startYear, startMonth, endYear, endMonth,):
        self.pageid = pageName
        self.lang = lang
        self.startYear = startYear
        self.startMonth = startMonth
        self.endYear = endYear
        self.endMonth = endMonth 
        self.revision = {}
        self.revision[self.pageid] = {}

    def revisionTimestamp(self, revid):
        url = "https://{}.wikipedia.org//w/api.php?action=query&format=json&prop=revisions&revids={}&rvprop=timestamp&rvslots=*".format(
            self.lang, revid)
        response = urllib.request.urlopen(url).read().decode("utf-8")
        res = ast.literal_eval(response)
        timestamp = nested_lookup('timestamp', res)
        return timestamp[0]

    def parseDiffHtml(self, html, revid=None):
        deleted = []
        add = []
        try:
            deletedarray = re.findall(
                r'<td class=\"diff-deletedline\"><div>(.*)</div></td>', html)
            for d in deletedarray:
                deleted.append(d)
        except:
            print("Fail at {}".format(revid))
            pass
        try:
            addarray = re.findall(
                r'<td class=\"diff-addedline\"><div>(.*)</div></td>', html)
            for d in addarray:
                add.append(d)
        except:
            print("Fail at {}".format(revid))
            pass
        return deleted, add

    def oldestRevision(self):
        date = "{}-{}-01T00%3A00%3A00.000Z".format(
            self.startYear, self.startMonth)
        url = "https://{}.wikipedia.org/w/api.php?action=query&format=json&prop=revisions&titles={}&rvprop=timestamp%7Cuser%7Cids%7Ccomment&rvlimit=1&rvstart={}&rvdir=newer".format(
            self.lang, self.pageid, date)
        # 1990-05-01T00%3A00%3A00.000Z
        response = urllib.request.urlopen(url).read().decode("utf-8")
        res = ast.literal_eval(response)
        revid = nested_lookup('revid', res)
        return revid[0]

    def recentRevision(self):
        maxday = monthrange(self.endYear, self.endMonth)[1]
        date = "{}-{}-{}T00%3A00%3A00.000Z".format(
            self.endYear, self.endMonth, maxday)
        url = "https://{}.wikipedia.org/w/api.php?action=query&format=json&prop=revisions&titles={}&rvprop=timestamp%7Cuser%7Cids%7Ccomment&rvlimit=1&rvstart={}&rvdir=newer".format(
            self.lang, self.pageid, date)
        # 1990-05-01T00%3A00%3A00.000Z
        response = urllib.request.urlopen(url).read().decode("utf-8")
        res = ast.literal_eval(response)
        revid = nested_lookup('revid', res)
        return revid[0]

    def newestRevision(self):
        url = "https://{}.wikipedia.org//w/api.php?action=query&format=json&prop=revisions&titles={}&rvprop=timestamp%7Cuser%7Ccomment%7Cids&rvlimit=1".format(
            self.lang, self.pageid)
        # 1990-05-01T00%3A00%3A00.000Z
        response = urllib.request.urlopen(url).read().decode("utf-8")
        res = ast.literal_eval(response)
        revid = nested_lookup('revid', res)
        return revid[0]

    def getCompare(self, revid, prev=False):
        if prev == False:
            torelative = 'next'
        else:
            torelative = 'prev'
        url = "https://{}.wikipedia.org/w/api.php?action=compare&format=json&fromrev={}&torelative={}&topst=1&prop=ids%7Ctitle%7Cdiff%7Cuser%7Cparsedcomment".format(
            self.lang, revid, torelative)
        response = urllib.request.urlopen(url).read().decode("utf-8")
        res = ast.literal_eval(response)
        return res

    def extract(self):
        oldestrev = self.oldestRevision()
        if self.endYear and self.endMonth:
            newestrev = self.recentRevision()
        else:
            newestrev = self.newestRevision()
        it = 0
        comp = self.getCompare(oldestrev, prev=True)
        deleted, added = self.parseDiffHtml(comp['compare']['*'])
        self.revision[self.pageid][it] = {'username': comp['compare']['touser'],
                                          'userid': comp['compare']['touserid'],
                                          'revid': comp['compare']['torevid'],
                                          'timestamp': self.revisionTimestamp(oldestrev),
                                          'comment': comp['compare']['toparsedcomment'],
                                          'deleted': deleted,
                                          'added': added}
        revid = oldestrev

        while revid != newestrev:  # get compare between a revision and the wext, until the newest is reached
            it += 1
            comp = self.getCompare(revid, prev=False)
            # print(comp)
            try:
                deleted, added = self.parseDiffHtml(
                    comp['compare']['*'], revid)
                self.revision[self.pageid][it] = {'username': comp['compare']['touser'],
                                                  'userid': comp['compare']['touserid'],
                                                  'revid': comp['compare']['torevid'],
                                                  'timestamp': self.revisionTimestamp(revid),
                                                  'comment': comp['compare']['toparsedcomment'],
                                                  'deleted': deleted,
                                                  'added': added}

                revid = comp['compare']['torevid']
            except:
                break

    def to_json(self, outputPath):
        with open(outputPath, 'w') as fp:
            json.dump(self.revision, fp)
