def readArticleList(articlesListPath):
    with open(articlesListPath, 'r') as f:
        articles = [line.strip().replace(' ', '_') for line in f]
    return articles


def chunkIt(seq, n):
    avg = len(seq) / float(n)
    out = []
    last = 0.0
    while last < len(seq):
        out.append(seq[int(last):int(last + avg)])
        last += avg
    return out


def timer(function):
    def new_function():
        start_time = timeit.default_timer()
        function()
        elapsed = timeit.default_timer() - start_time
        logger.info('Function "{name}" took {time} seconds to complete.'.format(
            name=function.__name__, time=elapsed))
    return new_function()
